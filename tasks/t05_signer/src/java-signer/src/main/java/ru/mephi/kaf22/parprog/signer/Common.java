package ru.mephi.kaf22.parprog.signer;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.FlowableProcessor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.PureJavaCrc32;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class Common {
    public static final int MAX_INPUT_DATA_LENGTH = 100;

    public static AtomicInteger dataSignerOverheat = new AtomicInteger(0);

    public static String DataSignerSalt = "";

    public static Runnable OverheatLock = () -> {
        while (true) {
            var swapped = dataSignerOverheat.compareAndSet(0, 1);
            if (!swapped) {
                System.out.println("OverheatLock happend");
                sleep(1000);
            } else {
                break;
            }
        }
    };

    public static Runnable OverheatUnlock = () -> {
        while (true) {
            var swapped = dataSignerOverheat.compareAndSet(1, 0);
            if (!swapped) {
                System.out.println("OverheatUnlock happend");
                sleep(1000);
            } else {
                break;
            }
        }
    };

    public static Function<String, String> DataSignerMd5 = (input) -> {
        OverheatLock.run();
        var result = DigestUtils.md5Hex(input).toLowerCase();
        sleep(10);
        OverheatUnlock.run();
        return result;
    };

    public static Function<String, String> DataSignerCrc32 = (input) -> {
        var crc32 = new PureJavaCrc32();
        crc32.update(input.getBytes(StandardCharsets.UTF_8));
        var result = crc32.getValue();
        sleep(1000);
        return String.format("%d", result);
    };

    public interface Job {
        void exec(FlowableProcessor<Object> in, FlowableProcessor<Object> out);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignored) {
        }
    }
}
