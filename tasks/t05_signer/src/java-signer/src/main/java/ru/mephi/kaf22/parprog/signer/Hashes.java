package ru.mephi.kaf22.parprog.signer;

import io.reactivex.rxjava3.processors.FlowableProcessor;

import java.util.concurrent.atomic.AtomicLong;

public class Hashes {
    // для трассировки времени выполнения функций
    private static AtomicLong start = new AtomicLong(System.currentTimeMillis());

    public static void singleHash(FlowableProcessor<Object> in, FlowableProcessor<Object> out) {
        // TODO: implement
    }

    public static void multiHash(FlowableProcessor<Object> in, FlowableProcessor<Object> out) {
        // TODO: implement
    }

    public static void combineResults(FlowableProcessor<Object> in, FlowableProcessor<Object> out) {
        // TODO: implement
    }
}
