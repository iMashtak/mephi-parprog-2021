package ru.mephi.kaf22.parprog.signer;

import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.PureJavaCrc32;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static ru.mephi.kaf22.parprog.signer.Common.*;

public class Tests {


    /**
     * Тест проверяет, что Pipeline не пытается выполнить все стадии последовательно, накапливая значения.
     * Необходимо, чтобы результаты предыдущей функции передавались в следующую по мере их вычисления, без
     * ожидания, пока все предыдущие результаты вычислятся.
     */
    @Test
    public void testPipeline() {
        var ok = new AtomicBoolean(true);
        var received = new AtomicInteger(0);

        var jobs = List.<Job>of(
                (in, out) -> {
                    sleep(100);
                    out.onNext(1);
                    if (received.get() == 0) {
                        ok.set(false);
                    }
                },
                (in, out) -> {
                    in.subscribe(x -> {
                        received.set((Integer) x);
                    });
                }
        );

        var pipe = new ExecutionPipeline();
        pipe.execute(jobs);

        if (!ok.get() || received.get() == 0) {
            Assertions.fail("Pipeline works wrong");
        }
    }

    @Test
    public void testSigner() {
        var expectedValue =
                "1173136728138862632818075107442090076184424490584241521304_1696913515191343735512658979631549563179965036907783101867_27225454331033649287118297354036464389062965355426795162684_29568666068035183841425683795340791879727309630931025356555_3994492081516972096677631278379039212655368881548151736_4958044192186797981418233587017209679042592862002427381542_4958044192186797981418233587017209679042592862002427381542";
        var expectedTime = 3500;
        var actualValue = new AtomicReference<>(null);

        var overheatLockCounter = new AtomicInteger(0);
        var overheatUnlockCounter = new AtomicInteger(0);
        var dataSignerMd5Counter = new AtomicInteger(0);
        var dataSignerCrc32Counter = new AtomicInteger(0);

        OverheatLock = () -> {
            overheatLockCounter.incrementAndGet();
            while (true) {
                var swapped = dataSignerOverheat.compareAndSet(0, 1);
                if (!swapped) {
                    System.out.println("OverheatLock happend");
                    sleep(1000);
                } else {
                    break;
                }
            }
        };

        OverheatUnlock = () -> {
            overheatUnlockCounter.incrementAndGet();
            while (true) {
                var swapped = dataSignerOverheat.compareAndSet(1, 0);
                if (!swapped) {
                    System.out.println("OverheatUnlock happend");
                    sleep(1000);
                } else {
                    break;
                }
            }
        };

        DataSignerMd5 = (input) -> {
            dataSignerMd5Counter.incrementAndGet();
            OverheatLock.run();
            var result = DigestUtils.md5Hex(input).toLowerCase();
            sleep(10);
            OverheatUnlock.run();
            return result;
        };

        DataSignerCrc32 = (input) -> {
            dataSignerCrc32Counter.incrementAndGet();
            var crc32 = new PureJavaCrc32();
            crc32.update(input.getBytes(StandardCharsets.UTF_8));
            var result = crc32.getValue();
            sleep(1000);
            return String.valueOf(result);
        };

        var inputData = List.of(0, 1, 1, 2, 3, 5, 8);

        var jobs = List.<Job>of(
                (in, out) -> {
                    for (var item : inputData) {
                        out.onNext(item);
                    }
                    out.onComplete();
                },
                Hashes::singleHash,
                Hashes::multiHash,
                Hashes::combineResults,
                (in, out) -> in.subscribe(actualValue::set)
        );

        var pipe = new ExecutionPipeline();
        Instant start = Instant.now();
        pipe.execute(jobs);
        Instant finish = Instant.now();
        long actualTime = Duration.between(start, finish).toMillis();

        System.out.printf("actualTime=%dms, expectedTime=%dms%n", actualTime, expectedTime);
        Assertions.assertTrue(actualTime < expectedTime);

        Assertions.assertEquals(expectedValue, actualValue.get());

        Assertions.assertEquals(overheatLockCounter.get(), inputData.size());
        Assertions.assertEquals(overheatUnlockCounter.get(), inputData.size());
        Assertions.assertEquals(dataSignerMd5Counter.get(), inputData.size());
        Assertions.assertEquals(dataSignerCrc32Counter.get(), inputData.size() * 8);
    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ignored) {
        }
    }
}
