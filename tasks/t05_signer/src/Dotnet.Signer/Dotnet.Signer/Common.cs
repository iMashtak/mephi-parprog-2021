using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Force.Crc32;

namespace Dotnet.Signer
{
    public static class Common
    {
        public const int MaxInputDataLength = 100;

        public static int dataSignerOverheat = 0;

        public static string DataSignerSalt { get; set; } = "";

        public static Action OverheatLock { get; set; } = () =>
        {
            while (true)
            {
                var origin = Interlocked.CompareExchange(ref dataSignerOverheat, 1, 0);
                var swapped = origin == 0;
                if (!swapped)
                {
                    Console.WriteLine("OverheatLock happend");
                    Thread.Sleep(1000);
                }
                else
                {
                    break;
                }
            }
        };

        public static Action OverheatUnlock { get; set; } = () =>
        {
            while (true)
            {
                var origin = Interlocked.CompareExchange(ref dataSignerOverheat, 0, 1);
                var swapped = origin == 1;
                if (!swapped)
                {
                    Console.WriteLine("OverheatUnlock happend");
                    Thread.Sleep(1000);
                }
                else
                {
                    break;
                }
            }
        };

        public static Func<string, string> DataSignerMd5 { get; set; } = input =>
        {
            OverheatLock.Invoke();
            using var md5 = MD5.Create();
            var bytes = Encoding.UTF8.GetBytes(input + DataSignerSalt);
            var result = md5.ComputeHash(bytes);
            var output = Convert.ToHexString(result).ToLower();
            Thread.Sleep(10);
            OverheatUnlock.Invoke();
            return output;
        };

        public static Func<string, string> DataSignerCrc32 { get; set; } = input =>
        {
            var bytes = Encoding.UTF8.GetBytes(input + DataSignerSalt);
            var checksum = Crc32Algorithm.Compute(bytes);
            Thread.Sleep(1000);
            return checksum.ToString();
        };
        
        public delegate Task Job(ChannelReader<object> input, ChannelWriter<object> output);
    }
}