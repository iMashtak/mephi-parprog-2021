using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Force.Crc32;
using Xunit;
using Xunit.Abstractions;
using static Dotnet.Signer.Common;

namespace Dotnet.Signer.Tests
{
    public class Tests : Specification
    {
        /// <summary>
        /// Тест проверяет, что Pipeline не пытается выполнить все стадии последовательно, накапливая значения.
        /// Необходимо, чтобы результаты предыдущей функции передавались в следующую по мере их вычисления, без
        /// ожидания, пока все предыдущие результаты вычислятся.
        /// </summary>
        /// <exception cref="Exception"></exception>
        [Fact]
        public void TestPipeline()
        {
            var ok = true;
            var received = 0;
            var jobs = new Job[]
            {
                // Эта функция посылает значение в out-канал и ждёт, пока его там прочитают
                async (i, o) =>
                {
                    await o.WriteAsync(1);
                    await Task.Delay(10);
                    if (received == 0)
                    {
                        ok = false;
                    }
                },
                // Эта функция должна поймать значение и успеть увеличить счётчик за 10мс, в течение которых спит
                // предыдущая функция
                async (i, o) =>
                {
                    await foreach (var item in i.ReadAllAsync())
                    {
                        Interlocked.Increment(ref received);
                    }
                }
            };
            var pipe = new ExecutionPipeline();
            pipe.Execute(jobs).Wait();
            if (!ok || received == 0)
            {
                throw new Exception("Pipeline works wrong");
            }
        }

        [Fact]
        public void TestSigner()
        {
            var expectedValue =
                    "1173136728138862632818075107442090076184424490584241521304_1696913515191343735512658979631549563179965036907783101867_27225454331033649287118297354036464389062965355426795162684_29568666068035183841425683795340791879727309630931025356555_3994492081516972096677631278379039212655368881548151736_4958044192186797981418233587017209679042592862002427381542_4958044192186797981418233587017209679042592862002427381542";
            string actualValue = null;

            var overheatLockCounter = 0;
            var overheatUnlockCounter = 0;
            var dataSignerMd5Counter = 0;
            var dataSignerCrc32Counter = 0;

            OverheatLock = () =>
            {
                Interlocked.Increment(ref overheatLockCounter);
                while (true)
                {
                    var origin = Interlocked.CompareExchange(ref dataSignerOverheat, 1, 0);
                    var swapped = origin == 0;
                    if (!swapped)
                    {
                        Console.WriteLine("OverheatLock happend");
                    }
                    else
                    {
                        break;
                    }
                }
            };
            OverheatUnlock = () =>
            {
                Interlocked.Increment(ref overheatUnlockCounter);
                while (true)
                {
                    var origin = Interlocked.CompareExchange(ref dataSignerOverheat, 0, 1);
                    var swapped = origin == 1;
                    if (!swapped)
                    {
                        Console.WriteLine("OverheatUnlock happend");
                    }
                    else
                    {
                        break;
                    }
                }
            };
            DataSignerMd5 = input =>
            {
                Interlocked.Increment(ref dataSignerMd5Counter);
                OverheatLock.Invoke();
                using var md5 = MD5.Create();
                var bytes = Encoding.UTF8.GetBytes(input + DataSignerSalt);
                var result = md5.ComputeHash(bytes);
                var output = Convert.ToHexString(result).ToLower();
                Thread.Sleep(10);
                OverheatUnlock.Invoke();
                return output;
            };
            DataSignerCrc32 = input =>
            {
                Interlocked.Increment(ref dataSignerCrc32Counter);
                var bytes = Encoding.UTF8.GetBytes(input + DataSignerSalt);
                var checksum = Crc32Algorithm.Compute(bytes);
                Thread.Sleep(1000);
                return checksum.ToString();
            };

            var inputData = new[] { 0, 1, 1, 2, 3, 5, 8 };

            var jobs = new Job[]
            {
                async (_, o) =>
                {
                    foreach (var item in inputData) { await o.WriteAsync(item); }
                },
                Hashes.SingleHash,
                Hashes.MultiHash,
                Hashes.CombineResults,
                async (i, _) =>
                {
                    var result = await i.ReadAsync();
                    actualValue = Convert.ToString(result);
                }
            };

            var pipe = new ExecutionPipeline();

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            pipe.Execute(jobs).Wait();
            var actualTime = stopwatch.ElapsedMilliseconds;
            stopwatch.Stop();
            var expectedTime = 3500;

            Assert.Equal(expectedValue, actualValue);

            Assert.True(actualTime < expectedTime);

            Assert.Equal(overheatLockCounter, inputData.Length);
            Assert.Equal(overheatUnlockCounter, inputData.Length);
            Assert.Equal(dataSignerMd5Counter, inputData.Length);
            Assert.Equal(dataSignerCrc32Counter, inputData.Length * 8);
        }

        public Tests(ITestOutputHelper testOutputHelper) : base(testOutputHelper) { }
    }
}