using System.IO;
using System.Text;
using Xunit.Abstractions;

namespace Dotnet.Signer.Tests
{
    public class ConsoleOutputConverter : TextWriter
    {
        ITestOutputHelper _output;
        public ConsoleOutputConverter(ITestOutputHelper output)
        {
            this._output = output;
        }
        public override Encoding Encoding => Encoding.Default;

        public override void WriteLine(string? message)
        {
            this._output.WriteLine(message);
        }
        public override void WriteLine(string format, params object?[] args)
        {
            this._output.WriteLine(format, args);
        }
    }

}