# Шаблон курсовой работы по параллельному программированию в $`\LaTeX`$ с использованием утилиты `lpt`

Шаблон подготовлен при помощи утилиты `lpt`: [gitlab.com/iMashtak/lpt-tool](https://gitlab.com/iMashtak/lpt-tool).

## Если нет желания разбираться с `lpt`

В папке `src/pp-2021-report` уже подготовлен шаблон с настроенными стилями. Любым билд-тулом можно компилировать корневой файл `Г00-000_ФамилияИО_КРА.tex` и получить `.pdf`-файл. Для корректной сборки рекомендуется использовать `latexmk -xelatex` или аналогичные по принципу команды.

## Если есть желание попробовать использовать `lpt`

1. Установите утилиту `lpt`: [gitlab.com/iMashtak/lpt-tool](https://gitlab.com/iMashtak/lpt-tool).
2. Выполните команды:
    ```bash
    lpt workspace update
    lpt document update pp-2021-report
    ```
3. Выполните сборку документа средствами VSCode (первая сборка может быть долгой из-за скачивания docker-образа с латехом).
