﻿using System;
using System.Threading;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Running;
using Dotnet.DataStructures;

namespace Dotnet.DataStructs.Benchmarks
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var _ = BenchmarkRunner.Run<MatrixResearch>();
        }
    }

    [MemoryDiagnoser]
    [ThreadingDiagnoser]
    [SimpleJob(RunStrategy.Throughput)]
    public class MatrixResearch
    {
        int[,]         m1     = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
        int[,]         m2     = { { 10, 20, 30 }, { 40, 50, 60 }, { 70, 80, 90 }, { 100, 110, 120 } };
        private int[,] result = new int[4,3];
    
        [Benchmark]
        public void Classic() { NaiveMatrixSummator.SumClassic(this.m1, this.m2, this.result); }
    
        [Benchmark]
        public void Threading() { NaiveMatrixSummator.SumUsingThreads(this.m1, this.m2, this.result); }
    }

    [MemoryDiagnoser]
    [SimpleJob(
        RunStrategy.Throughput,
        launchCount: 1,
        warmupCount: 10,
        targetCount: 5
    )]
    public class ThreadPoolTuning
    {
        private int[,] _m1;
        private int[,] _m2;
        private int[,] _result;

        [Params(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)]
        public int _tc;

        // public int _tc = 5;

        // [Params("250:1000", "250:5000", "250:10000", "100:1000", "500:1000", "50:200")] public string _dims;
        public string _dims = "100:1000";

        public MatrixSummatorObject _matrixSummatorObject;

        [GlobalSetup]
        public void Setup()
        {
            this._matrixSummatorObject = new MatrixSummatorObject(this._tc);
            var lines = Convert.ToInt32(this._dims.Split(":")[0]);
            var columns = Convert.ToInt32(this._dims.Split(":")[1]);
            this._m1 = new int[lines, columns];
            var rnd = new Random();
            for (var i = 0; i < this._m1.Length; i++)
            {
                var x = i / columns;
                var y = i % columns;
                this._m1[x, y] = rnd.Next(1000);
            }
            this._m2 = new int[lines, columns];
            for (var i = 0; i < this._m2.Length; i++)
            {
                var x = i / columns;
                var y = i % columns;
                this._m2[x, y] = rnd.Next(1000);
            }
            this._result = new int[lines, columns];
        }

        [GlobalCleanup]
        public void Cleanup() { this._matrixSummatorObject.Dispose(); }

        [Benchmark]
        public void ThreadPool() { this._matrixSummatorObject.Sum(this._m1, this._m2, this._result); }

        [Benchmark]
        public void NoThreads() { NaiveMatrixSummator.SumClassic(this._m1, this._m2, this._result); }
    }
}