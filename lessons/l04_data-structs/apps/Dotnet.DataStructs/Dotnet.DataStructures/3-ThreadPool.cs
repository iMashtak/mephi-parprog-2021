using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Dotnet.DataStructures
{
    public class ThreadPool
    {
        private ConcurrentQueue<(Action<object?> Action, object? Value)> _queue;
        private List<Thread>                                             _threads;
        private CountdownEvent                                           _waitGroup;

        private volatile bool _isAlive;

        public ThreadPool(int size)
        {
            this._queue = new ConcurrentQueue<(Action<object?> Action, object? Value)>();
            this._threads = Enumerable.Repeat(0, size).Select(
                _ => new Thread(this.Working)
                {
                    IsBackground = true
                }
            ).ToList();
            this._waitGroup = new CountdownEvent(size);
            this._isAlive = true;
            foreach (var thread in this._threads)
            {
                thread.Start();
            }
        }

        public void Kill()
        {
            this._isAlive = false;
            this._waitGroup.Wait();
        }

        public void Submit(Action action) { this._queue.Enqueue((_ => action.Invoke(), null)); }

        public void Submit(Action<object?> action, object? value)
        {
            this._queue.Enqueue((action, value));
        }

        private void Working()
        {
            while (this._isAlive)
            {
                var ok = this._queue.TryDequeue(out var entry);
                if (ok) { entry.Action.Invoke(entry.Value); }
            }
            this._waitGroup.Signal();
        }
    }

    public class MatrixSummatorObject : IDisposable
    {
        private ThreadPool _threadPool;

        public MatrixSummatorObject(int poolSize) { this._threadPool = new ThreadPool(poolSize); }

        public void Sum(int[,] left, int[,] right, int[,] result)
        {
            if (left.Length != right.Length) { throw new ArgumentException("Unequal length"); }
            var lines = left.GetUpperBound(0) + 1;
            var columns = left.GetUpperBound(1) + 1;
            for (var i = 0; i < lines; i++)
            {
                this._threadPool.Submit(
                    (x) =>
                    {
                        var wg = new CountdownEvent(10);
                        for (var o = 0; o < 10; o++)
                        {
                            this._threadPool.Submit(() => { wg.Signal(); });
                        }
                        wg.Wait();
                        var i = (int)x;
                        for (var j = 0; j < columns; j++)
                        {
                            result[i, j] = left[i, j] + right[i, j];
                        }
                    },
                    i
                );
            }
        }

        public void Dispose()
        {
            this._threadPool.Kill();
        }
    }
}