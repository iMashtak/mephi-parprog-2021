using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Dotnet.DataStructures
{
    public class LockFreeStack<T>
    {
        private class Node
        {
            public T     Item { get; }
            public Node? Next { get; set; }
            public Node(T item) { this.Item = item; }
        }

        private volatile Node? _head;

        public void Push(T item)
        {
            var node = new Node(item);
            // var spinWait = new SpinWait();
            Node? originHead;
            do
            {
                // Thread.Sleep(1);
                // spinWait.SpinOnce(); // похоже на Thread.Sleep, только легче
                node.Next = this._head;
                originHead = Interlocked.CompareExchange(ref this._head, node, node.Next);
            } while (originHead != node.Next);
        }

        private Node _compareExchange(ref Node location, Node value, Node compared)
        {
            var origin = location;
            if (location == compared)
            {
                location = value;
            }
            return origin;
        }

        public int Count
        {
            get
            {
                if (this._head == null) { return 0; }
                var counter = 0;
                for (var node = this._head; node != null; node = node.Next)
                {
                    counter++;
                }
                return counter;
            }
        }
    }

    public class LockFreeStackExample
    {
        public static void Show()
        {
            var s = new LockFreeStack<int>();
            // var s = new Stack<int>();
            var c = new CountdownEvent(16);
            var threads = Enumerable.Range(0, 16).Select(
                _ => new Thread(
                    v =>
                    {
                        for (var i = 0; i < 50; i++)
                        {
                            s.Push((int)v);
                        }
                        c.Signal();
                    }
                )
            );
            foreach (var thread in threads)
            {
                thread.Start(10);
            }
            c.Wait();
            Console.WriteLine($"Stack depth: {s.Count}");
        }
    }
}