using System;
using System.Linq;
using System.Threading;

namespace Dotnet.DataStructures
{
    public class NaiveMatrixSummator
    {
        public static void SumClassic(int[,] left, int[,] right, int[,] result)
        {
            if (left.Length != right.Length) { throw new ArgumentException("Unequal length"); }
            for (var i = 0; i < left.GetUpperBound(0) + 1; i++)
            {
                for (var j = 0; j < left.GetUpperBound(1) + 1; j++)
                {
                    result[i, j] = left[i, j] + right[i, j];
                }
            }
        }

        public static void SumUsingThreads(int[,] left, int[,] right, int[,] result)
        {
            if (left.Length != right.Length) { throw new ArgumentException("Unequal length"); }
            var wg = new CountdownEvent(left.Length);
            var lines = left.GetUpperBound(0) + 1;
            var columns = left.GetUpperBound(1) + 1;
            var threads = Enumerable.Range(0, left.Length).Select(
                i =>
                    new Thread(
                        () =>
                        {
                            var x = i / columns;
                            var y = i % columns;
                            result[x, y] = left[x, y] + right[x, y];
                            wg.Signal();
                        }
                    )
            );
            foreach (var thread in threads) {thread.Start();}
            wg.Wait();
        }
    }
}