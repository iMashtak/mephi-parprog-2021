﻿using System;
using Dotnet.DataStructures;

namespace Dotnet.DataStructs.Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            var threadPool = new ThreadPool(10);
            threadPool.Kill();
        }
    }
}