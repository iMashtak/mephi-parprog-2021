package ru.mephi.kaf22.parprog.rx_05_pipelines;

import io.reactivex.rxjava3.processors.FlowableProcessor;
import io.reactivex.rxjava3.processors.ReplayProcessor;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subscribers.DefaultSubscriber;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class CountWords {
    private static ConcurrentHashMap<Integer, Integer> countWordsResults = new ConcurrentHashMap<>();

    @SneakyThrows
    public static void main(String[] args) {
        var cdl = new CountDownLatch(1);

        var fileProcessor = ReplayProcessor.<String>create();
        var countLinesProcessor = ReplayProcessor.<Integer>create();

        var executor = Executors.newFixedThreadPool(3);

        executor.submit(() -> readFile("file.txt", fileProcessor));
        executor.submit(() -> countWords(fileProcessor, cdl));
        executor.submit(() -> countLines(fileProcessor, countLinesProcessor));

        countLinesProcessor.firstElement().subscribe(System.out::println);
        executor.shutdown();
    }

    @SneakyThrows
    private static void readFile(String path, FlowableProcessor<String> out) {
        var streamReader = new InputStreamReader(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(path),
                StandardCharsets.UTF_8
        );
        var reader = new BufferedReader(streamReader);
        for (String line; (line = reader.readLine()) != null; ) {
            out.onNext(line);
        }
        out.onComplete();
    }

    private static void countWords(FlowableProcessor<String> processor, CountDownLatch cdl) {
        var i = new AtomicInteger(0);
        processor.observeOn(Schedulers.computation(), false, 10)
                .subscribe(new DefaultSubscriber<>() {
                    @Override
                    public void onNext(String line) {
                        countWordsResults.put(
                                i.getAndIncrement(),
                                Arrays.stream(line.split(" "))
                                        .filter(x -> x != null && !x.equals(""))
                                        .map(x -> 1)
                                        .reduce(0, Integer::sum)
                        );
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {
                        cdl.countDown();
                    }
                });
    }

    private static void countLines(FlowableProcessor<String> in, FlowableProcessor<Integer> out) {
        var result = new AtomicInteger(0);
        in.observeOn(Schedulers.computation(), false, 10)
                .subscribe(new DefaultSubscriber<>() {
                    @Override
                    public void onNext(String s) {
                        result.incrementAndGet();
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {
                        out.onNext(result.get());
                    }
                });
    }
}
