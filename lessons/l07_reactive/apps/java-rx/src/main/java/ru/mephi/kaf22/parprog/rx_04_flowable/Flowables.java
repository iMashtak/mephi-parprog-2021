package ru.mephi.kaf22.parprog.rx_04_flowable;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.exceptions.MissingBackpressureException;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.SneakyThrows;

public class Flowables {
    @SneakyThrows
    public static void main(String[] args) {
        var backpressureStrategy = BackpressureStrategy.DROP;
        var flowable = Flowable.create(subscriber -> {
            for (var i = 0; i < 200; i++) {
                try {
                    subscriber.onNext(i);
                } catch (MissingBackpressureException e) {
                    subscriber.onError(e);
                    return;
                }
            }
            subscriber.onComplete();
        }, backpressureStrategy);

        flowable.observeOn(Schedulers.computation(), false, 500)
                .subscribe(
                        System.out::println,
                        (x) -> {
                            System.out.println("ERROR " + x.getMessage());
                        }
                );

        Thread.sleep(1000);
    }
}
