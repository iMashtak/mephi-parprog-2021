package ru.mephi.kaf22.parprog.rx_01_basics;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observers.DefaultObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.SneakyThrows;

import java.util.concurrent.CountDownLatch;

public class Basics {
    public static void main(String[] args) throws InterruptedException {
//        simpleObservable();
        multipleSubscriptions();
    }

    @SneakyThrows
    public static void simpleObservable() {
        var cdl = new CountDownLatch(1);
        var observable = Observable
                .<Integer>create(
                        subscriber -> {
                            System.out.printf("Create: thread = %s%n", Thread.currentThread().getName());
                            for (var i = 0; i < 5; i++) {
                                subscriber.onNext(i);
                            }
                            subscriber.onComplete();
                        }
                );
        observable = observable
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.newThread())
        ;
        observable.subscribe(new DefaultObserver<>() {
            @Override
            public void onNext(@NonNull Integer x) {
                System.out.printf("Receive: thread = %s, x = %d%n", Thread.currentThread().getName(), x);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                System.out.printf("Complete: thread = %s%n", Thread.currentThread().getName());
                cdl.countDown();
            }
        });
        cdl.await();
    }

    @SneakyThrows
    public static void multipleSubscriptions() {
        var start = System.currentTimeMillis();
        var cdl = new CountDownLatch(2);
        var observable = Observable
                .<Integer>create(
                        subscriber -> {
                            System.out.printf("Create: thread = %s, time=%dms%n", Thread.currentThread().getName(), System.currentTimeMillis() - start);
                            for (var i = 0; i < 5; i++) {
                                System.out.printf("Before next: time=%dms%n", System.currentTimeMillis() - start);
                                subscriber.onNext(i);
                                System.out.printf("After next: time=%dms%n", System.currentTimeMillis() - start);
                                Thread.sleep(100);
                            }
                            subscriber.onComplete();
                        }
                );

        observable = observable.observeOn(Schedulers.newThread());
        observable = observable.subscribeOn(Schedulers.newThread());

        System.out.printf("Creating first subscription: time=%dms%n", System.currentTimeMillis() - start);
        observable.subscribe(new DefaultObserver<>() {
            @SneakyThrows
            @Override
            public void onNext(@NonNull Integer x) {
                System.out.printf("1 :: thread = %s, x = %d%n", Thread.currentThread().getName(), x);
                Thread.sleep(100);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                cdl.countDown();
            }
        });

        System.out.printf("Creating second subscription: time=%dms%n", System.currentTimeMillis() - start);
        observable.subscribe(new DefaultObserver<>() {
            @SneakyThrows
            @Override
            public void onNext(@NonNull Integer x) {
                System.out.printf("2 :: thread = %s, x = %d%n", Thread.currentThread().getName(), x);
                Thread.sleep(200);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                cdl.countDown();
            }
        });

        cdl.await();
        System.out.printf("Endian: time=%dms%n", System.currentTimeMillis() - start);
    }
}
