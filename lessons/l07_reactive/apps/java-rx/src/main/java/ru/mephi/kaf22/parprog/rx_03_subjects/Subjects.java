package ru.mephi.kaf22.parprog.rx_03_subjects;

import io.reactivex.rxjava3.subjects.AsyncSubject;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.ReplaySubject;
import lombok.SneakyThrows;

public class Subjects {
    @SneakyThrows
    public static void main(String[] args) {
        var subject = ReplaySubject.create();
//        var subject = PublishSubject.create();
//        var subject = BehaviorSubject.create();
//        var subject = AsyncSubject.create();
        subject.subscribe(x -> System.out.println("First: " + x));
        subject.onNext(100);
        subject.onNext(500);
        subject.subscribe(x -> System.out.println("Second: " + x));
        subject.onNext("kek");
        subject.onNext("lol");
        subject.onComplete();
    }
}
