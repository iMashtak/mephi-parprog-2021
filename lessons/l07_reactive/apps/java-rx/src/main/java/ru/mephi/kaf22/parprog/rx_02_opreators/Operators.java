package ru.mephi.kaf22.parprog.rx_02_opreators;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.SneakyThrows;

import java.util.concurrent.CountDownLatch;

public class Operators {
    public static void main(String[] args) {
//        sampleChaining();
//        nontrivialChaining();
    }

    @SneakyThrows
    public static void sampleChaining() {
        var observable = Observable.fromArray(1, 2, 3, 4, 5, 6);
        var result = observable
                .map(x -> x)
                .reduce(0, (acc, item) -> acc * 10 + item);
        result.subscribe(x -> {
            System.out.println(x);
        });
    }

    @SneakyThrows
    public static void nontrivialChaining() {
        var cdl = new CountDownLatch(1);
        var observable = Observable.create(subscriber -> {
            for (var i = 0; i < 30; i++) {
                subscriber.onNext(i);
            }
            subscriber.onComplete();
        });
        observable
                .observeOn(Schedulers.io())
                .buffer(5)
                .map(x -> {
                    System.out.printf("Thread: %s%n", Thread.currentThread().getName());
                    return x.get(0);
                })
                .count()
                .subscribe(x -> {
                    System.out.println(x);
                    cdl.countDown();
                });

        cdl.await();
    }
}
