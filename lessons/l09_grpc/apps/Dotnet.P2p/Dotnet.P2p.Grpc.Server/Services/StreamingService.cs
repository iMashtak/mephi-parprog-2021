using System;
using System.Threading.Tasks;
using Dotnet.P2p.Grpc.Services.Data;
using Dotnet.P2p.Grpc.Services.Streaming;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Dotnet.P2p.Grpc.Server.Services
{
    public class StreamingServiceImpl : StreamingService.StreamingServiceBase
    {
        public override async Task<StringReply> DoUnary(StringRequest request, ServerCallContext context)
        {
            Console.WriteLine("-- Unary --");
            await Task.Delay(1);
            Console.WriteLine(request);
            return new StringReply
            {
                Reply = "reply"
            };
        }

        public override async Task DoServerStreaming(
            StringRequest                    request,
            IServerStreamWriter<StringReply> responseStream,
            ServerCallContext                context
        )
        {
            Console.WriteLine("-- Server Streaming --");
            for (var i = 0; i < 10; i++)
            {
                var msg = $"reply {i}";
                Console.WriteLine(msg);
                await responseStream.WriteAsync(
                    new StringReply
                    {
                        Reply = msg
                    }
                );
            }
        }

        public override async Task<StringReply> DoClientStreaming(
            IAsyncStreamReader<StringRequest> requestStream,
            ServerCallContext                 context
        )
        {
            Console.WriteLine("-- Client Streaming --");
            await foreach (var request in requestStream.ReadAllAsync())
            {
                Console.WriteLine(request);
            }
            return new StringReply
            {
                Reply = "ok"
            };
        }

        public override async Task DoBidirectionalStreaming(
            IAsyncStreamReader<StringRequest> requestStream,
            IServerStreamWriter<StringReply>  responseStream,
            ServerCallContext                 context
        )
        {
            Console.WriteLine("-- Bidirectional Streaming --");
            await foreach (var request in requestStream.ReadAllAsync())
            {
                Console.WriteLine(request);
                await responseStream.WriteAsync(
                    new StringReply
                    {
                        Reply = $"reply: {request.Request}"
                    }
                );
            }
        }

    }
}