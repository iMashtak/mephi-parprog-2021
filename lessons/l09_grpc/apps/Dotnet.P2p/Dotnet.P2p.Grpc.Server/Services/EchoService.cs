using System;
using System.Threading.Tasks;
using Dotnet.P2p.Grpc.Services.Data;
using Dotnet.P2p.Grpc.Services.Echo;
using Grpc.Core;

namespace Dotnet.P2p.Grpc.Server.Services
{
    public class EchoServiceImpl : EchoService.EchoServiceBase
    {
        public override async Task<StringReply> Echo(StringRequest request, ServerCallContext context)
        {
            Console.WriteLine("Receiving request");
            return new StringReply
            {
                Reply = request.Request
            };
        }
    }
}