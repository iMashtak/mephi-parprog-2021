﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Dotnet.P2p.Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = File.ReadAllText("../../../msg.json");
            var value = JsonSerializer.Deserialize<Person>(
                json
                // new JsonSerializerOptions()
                // {
                //     IgnoreNullValues = true
                // }
            );
            Console.WriteLine();
            // var value = new Person()
            // {
            //     Name = "Who",
            //     Age = 115
            // };
            // var json = JsonSerializer.Serialize(
            //     value,
            //     new JsonSerializerOptions()
            //     {
            //         IgnoreNullValues = true
            //     }
            // );
            // Console.WriteLine(json);
        }
    }

    class Person
    {
        [JsonPropertyName("name")] public string Name { get; init; }

        [JsonPropertyName("age")] public int Age { get; init; }

        [JsonPropertyName("is_student")] public bool IsStudent { get; init; }

        [JsonPropertyName("well_known_prog_langs")]
        public List<string> WellKnownProgLangs { get; init; }

        [JsonPropertyName("education")] public Education Education { get; init; }

        [JsonPropertyName("additional_info")] public object AdditionalInfo { get; init; }
    }

    class Education
    {
        [JsonPropertyName("university")] public string University { get; init; }

        [JsonPropertyName("department")] public string Department { get; init; }
    }
}