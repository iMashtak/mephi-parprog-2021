﻿using System;
using System.Threading.Tasks;
using Dotnet.P2p.Grpc.Services.Data;
using Dotnet.P2p.Grpc.Services.Echo;
using Dotnet.P2p.Grpc.Services.Streaming;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging.Abstractions;

namespace Dotnet.P2p.Grpc.Client
{
    internal class Program
    {
        internal static async Task Main(string[] args)
        {
            // ОЧЕНЬ ВАЖНАЯ НАСТРОЙКА
            // Отключает обязательное шифрование соединения
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using var channel = GrpcChannel.ForAddress(
                "http://localhost:5000",
                new GrpcChannelOptions()
                {
                    Credentials = ChannelCredentials.Insecure,
                    LoggerFactory = new NullLoggerFactory()
                }
            );
            // await SimpleExample(channel);
            await StreamingExample(channel);
        }

        private static async Task SimpleExample(ChannelBase channel)
        {
            var echo = new EchoService.EchoServiceClient(channel);
            var reply = await echo.EchoAsync(
                new StringRequest()
                {
                    Request = "Hello!"
                }
            );
            Console.WriteLine("Ответ сервера: " + reply.Reply);
        }

        private static async Task StreamingExample(ChannelBase channel)
        {
            var streamingService = new StreamingService.StreamingServiceClient(channel);

            Console.WriteLine("-- Unary --");
            var unaryRequest = new StringRequest
            {
                Request = "request"
            };
            Console.WriteLine(unaryRequest);
            var unaryReply = await streamingService.DoUnaryAsync(unaryRequest);
            Console.WriteLine(unaryReply);

            //------------------------

            Console.WriteLine("-- Server Streaming --");
            var serverStreamingRequest = new StringRequest
            {
                Request = "request"
            };
            Console.WriteLine(serverStreamingRequest);
            var serverStreamingCall = streamingService.DoServerStreaming(serverStreamingRequest);
            var responseStream = serverStreamingCall.ResponseStream;
            await foreach (var reply in responseStream.ReadAllAsync())
            {
                Console.WriteLine(reply);
            }

            //------------------------

            Console.WriteLine("-- Client Streaming --");
            var clientStreamingCall = streamingService.DoClientStreaming();
            for (var i = 0; i < 10; i++)
            {
                await clientStreamingCall.RequestStream.WriteAsync(
                    new StringRequest
                    {
                        Request = $"request {i}"
                    }
                );
            }
            await clientStreamingCall.RequestStream.CompleteAsync();
            var replyClientStreaming = await clientStreamingCall.ResponseAsync;
            Console.WriteLine(replyClientStreaming);

            //------------------------

            Console.WriteLine("-- Bidirectional Streaming --");
            var asyncDuplexStreamingCall = streamingService.DoBidirectionalStreaming();
            await asyncDuplexStreamingCall.RequestStream.WriteAsync(
                new StringRequest
                {
                    Request = "msg 1"
                }
            );
            await asyncDuplexStreamingCall.RequestStream.WriteAsync(
                new StringRequest()
                {
                    Request = "msg 2"
                }
            );
            ;
            if (await asyncDuplexStreamingCall.ResponseStream.MoveNext())
            {
                Console.WriteLine(asyncDuplexStreamingCall.ResponseStream.Current);
            }
            await asyncDuplexStreamingCall.RequestStream.CompleteAsync();
        }
    }
}