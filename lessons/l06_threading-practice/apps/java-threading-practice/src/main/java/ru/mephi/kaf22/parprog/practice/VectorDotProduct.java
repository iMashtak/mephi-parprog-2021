package ru.mephi.kaf22.parprog.practice;

import java.util.Arrays;
import java.util.concurrent.*;

public class VectorDotProduct {
    public static long calcDotProductParallelFixedBatch(
            long[] left,
            long[] right,
            int batchSize,
            ExecutorService executor
    ) {
        var waitGroup = new Phaser();
        var length = left.length;
        var results = new long[length / batchSize];
        for (var i = 0; i < length / batchSize; ++i) {
            int finalI = i;
            waitGroup.register();
            executor.submit(() -> {
                var from = finalI * batchSize;
                var to = Math.min(length, from + batchSize);
                var acc = 0L;
                for (var j = from; j < to; ++j) {
                    acc += left[j] * right[j];
                }
                results[finalI] = acc;
                waitGroup.arrive();
            });
        }
        waitGroup.awaitAdvance(0);
        return Arrays.stream(results).sum();
    }
}
