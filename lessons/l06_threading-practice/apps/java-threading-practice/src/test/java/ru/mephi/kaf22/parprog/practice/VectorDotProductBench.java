package ru.mephi.kaf22.parprog.practice;

import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@Fork(value = 1)
@Warmup(iterations = 3)
public class VectorDotProductBench {
    @Param({"1000000"})
    public int length;

    @Param({"1000"})
    public int batchSize;

    private long[] left;
    private long[] right;
    private static final ExecutorService executorService = Executors.newWorkStealingPool();

    @Setup
    public void setup() {
        var random = new Random();
        this.left = new long[this.length];
        this.right = new long[this.length];
        for (var i = 0; i < this.length; ++i)
        {
            this.left[i] = random.nextLong(100000);
            this.right[i] = random.nextLong(1000000);
        }
    }

    @TearDown
    public void tearDown() {
        executorService.shutdownNow();
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public void parallel() {
        VectorDotProduct.calcDotProductParallelFixedBatch(left, right, batchSize, executorService);
    }
}
