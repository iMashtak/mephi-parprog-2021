using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Dotnet.ThreadingPractice
{
    public class FindFile
    {
        public static long FindFileByNameOneThreaded(string path, Regex regex)
        {
            var directories = new Stack<string>(20);
            var result = 0L;
            directories.Push(path);

            while (directories.Count > 0)
            {
                var current = directories.Pop();
                var subDirectories = Directory.GetDirectories(current);
                var files = Directory.GetFiles(current);
                foreach (var file in files)
                {
                    var fileInfo = new FileInfo(file);
                    if (regex.IsMatch(fileInfo.Name))
                    {
                        Interlocked.Increment(ref result);
                    }
                }
                foreach (var dir in subDirectories)
                {
                    directories.Push(dir);
                }
            }

            return result;
        }

        public static async Task<long> FindFileByNameAsync(string path, Regex regex)
        {
            var directories = new ConcurrentStack<string>();
            directories.Push(path);
            var result = 0L;

            async IAsyncEnumerable<string> Stack()
            {
                while (!directories.IsEmpty)
                {
                    directories.TryPop(out var next);
                    yield return next;
                }
            }

            await foreach (var current in Stack())
            {
                await Task.WhenAll(
                    Task.Run(
                        () =>
                        {
                            var subDirectories = Directory.GetDirectories(current);
                            foreach (var dir in subDirectories)
                            {
                                directories.Push(dir);
                            }
                        }
                    ),
                    Task.Run(
                        () =>
                        {
                            var files = Directory.GetFiles(current);
                            Parallel.ForEach(
                                files,
                                file =>
                                {
                                    var fileInfo = new FileInfo(file);
                                    if (regex.IsMatch(fileInfo.Name))
                                    {
                                        Interlocked.Increment(ref result);
                                    }
                                }
                            );
                        }
                    )
                );
            }

            return result;
        }

        /// <summary>
        /// Независимые задачи:
        ///
        /// - обход папок
        /// - обхода файлов в конкретной папке
        /// </summary>
        /// <param name="path"></param>
        /// <param name="regex"></param>
        /// <returns></returns>
        public static async Task<long> FindFileByNameRecAsync(string path, Regex regex)
        {
            var result = 0L;

            async Task Rec(string current)
            {
                var fileTask = Task.Run(
                    () =>
                    {
                        var files = Directory.GetFiles(current);
                        Parallel.ForEach(
                            files,
                            file =>
                            {
                                var fileInfo = new FileInfo(file);
                                if (regex.IsMatch(fileInfo.Name))
                                {
                                    Interlocked.Increment(ref result);
                                }
                            }
                        );
                    }
                );
                var dirsTask = Task.WhenAll(
                    Directory
                        .GetDirectories(current)
                        .Select(dir => Rec(dir))
                );
                await Task.WhenAll(fileTask, dirsTask);
            }

            await Rec(path);

            return result;
        }

        public static long FindFileByNameLinearizatedAsync(string path, Regex regex)
        {
            var list = new List<string[]>();
            var result = 0L;

            void Rec(string current)
            {
                var subdirs = Directory.GetDirectories(current);
                list.Add(subdirs);
                foreach (var dir in subdirs)
                {
                    Rec(dir);
                }
            }

            list.Add(new[] { path });
            Rec(path);

            var files = list.AsParallel()
                .SelectMany(dirs => dirs.SelectMany(dir => Directory.GetFiles(dir)));
            Parallel.ForEach(
                files,
                file =>
                {
                    var fileInfo = new FileInfo(file);
                    if (regex.IsMatch(fileInfo.Name))
                    {
                        Interlocked.Increment(ref result);
                    }
                }
            );
            return result;
        }
    }
}