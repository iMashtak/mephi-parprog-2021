using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dotnet.ThreadingPractice
{
    public class VectorDotProduct
    {
        public static long CalcDotProductOneThread(long[] left, long[] right)
        {
            var acc = 0L;
            for (var i = 0; i < left.Length; ++i)
            {
                acc += left[i] * right[i];
            }
            return acc;
        }

        public static long CalcDotProductParallelFixedBatch(
            long[] left,
            long[] right,
            int    batchSize
        )
        {
            var length = left.Length;
            var results = new long[length / batchSize];
            Parallel.ForEach(
                Enumerable.Range(0, length / batchSize),
                i =>
                {
                    var from = i * batchSize;
                    var to = Math.Min(length, from + batchSize);
                    var acc = 0L;
                    for (var j = from; j < to; j++)
                    {
                        acc += left[j] * right[j];
                    }
                    results[i] = acc;
                }
            );
            return results.Sum();
        }

        public static long CalcDotProductParallelFixedRanges(
            long[] left,
            long[] right,
            int    rangesCount
        )
        {
            var length = left.Length;
            var results = new long[rangesCount];
            Parallel.ForEach(
                Enumerable.Range(0, rangesCount),
                i =>
                {
                    var from = i * (length / rangesCount);
                    var to = Math.Min(length, from + (length / rangesCount));
                    var acc = 0L;
                    for (var j = from; j < to; j++)
                    {
                        acc += left[j] * right[j];
                    }
                    results[i] = acc;
                }
            );
            return results.Sum();
        }
        
        public static long CalcDotProductWithParallelLinq(long[] left, long[] right)
        {
            return left.Zip(right).AsParallel().Aggregate(
                0L,
                (acc, tuple) => acc + tuple.First * tuple.Second
            );
        }
    }
}