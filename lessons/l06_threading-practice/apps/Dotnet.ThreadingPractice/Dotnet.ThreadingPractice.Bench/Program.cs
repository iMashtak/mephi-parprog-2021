﻿using System;
using BenchmarkDotNet.Running;

namespace Dotnet.ThreadingPractice.Bench
{
    class Program
    {
        static void Main(string[] args) { _ = BenchmarkRunner.Run<FindFileBenchmark>(); }
    }
}