using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;

namespace Dotnet.ThreadingPractice.Bench
{
    [MemoryDiagnoser]
    [SimpleJob(RunStrategy.Throughput, warmupCount: 5, targetCount: 5)]
    public class FindFileBenchmark
    {
        [Params("../../../../../../../../../../../../../")]
        public string Path;

        private Regex _regex = new Regex(@"^\..+");

        // [Benchmark]
        // public void OneThreaded()
        // {
        //     FindFile.FindFileByNameOneThreaded(this.Path, this._regex);
        // }

        [Benchmark]
        public async Task AsyncLike()
        {
            await FindFile.FindFileByNameAsync(this.Path, this._regex);
        }

        [Benchmark]
        public void LinearizatedAsync()
        {
            FindFile.FindFileByNameLinearizatedAsync(this.Path, this._regex);
        }
        
        [Benchmark]
        public async Task AsyncCustom()
        {
            await FindFile.FindFileByNameRecAsync(this.Path, this._regex);
        }
    }
}