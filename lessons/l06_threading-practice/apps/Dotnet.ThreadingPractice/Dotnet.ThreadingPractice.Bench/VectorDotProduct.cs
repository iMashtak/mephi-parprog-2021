using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;

namespace Dotnet.ThreadingPractice.Bench
{
    [MemoryDiagnoser]
    [SimpleJob(RunStrategy.Throughput, warmupCount: 5, targetCount: 5)]
    public class VectorDotProductBenchmark
    {
        [Params(1_000_000)] public int Length;

        private long[] left;
        private long[] right;

        [GlobalSetup]
        public void GlobalSetup()
        {
            var random = new Random();
            this.left = new long[this.Length];
            this.right = new long[this.Length];
            for (var i = 0; i < this.Length; ++i)
            {
                this.left[i] = random.Next(100000);
                this.right[i] = random.Next(1000000);
            }
        }

        [Benchmark]
        public void WithParallelLinq() { VectorDotProduct.CalcDotProductWithParallelLinq(this.left, this.right); }

        [Benchmark]
        public void OneThreaded() { VectorDotProduct.CalcDotProductOneThread(this.left, this.right); }

        // [Benchmark]
        // [Arguments(100)]
        // [Arguments(1_000)]
        // [Arguments(10_000)]
        // public void ParallelFixedBatch(int batchSize)
        // {
        //     VectorDotProduct.CalcDotProductParallelFixedBatch(this.left, this.right, batchSize);
        // }

        // [Benchmark]
        // [Arguments(6)]
        // [Arguments(8)]
        // public void ParallelFixedRanges(int ranges)
        // {
        //     VectorDotProduct.CalcDotProductParallelFixedRanges(this.left, this.right, ranges);
        // }
    }
}