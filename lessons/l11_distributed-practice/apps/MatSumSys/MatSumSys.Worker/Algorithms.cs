using MatSumSys.Protobuf.Matrix;

namespace MatSumSys.Worker
{
    public static class Algorithms
    {
        public static Matrix Sum(PairMatrix ms)
        {
            var left = ms.Left;
            var right = ms.Right;

            if (left.DimX != right.DimX || left.DimY != right.DimY)
            {
                // TODO
            }

            var dimX = left.DimX;
            var dimY = left.DimY;

            var result = new Matrix
            {
                DimX = dimX,
                DimY = dimY
            };

            for (var i = 0; i < dimX; ++i)
            {
                result.Lines.Add(new Matrix.Types.Line());
                for (var j = 0; j < dimY; ++j)
                {
                    var x = left.Lines[i].Columns[j];
                    var y = right.Lines[i].Columns[j];
                    result.Lines[i].Columns.Add(x + y);
                }
            }
            
            return result;
        }
    }
}