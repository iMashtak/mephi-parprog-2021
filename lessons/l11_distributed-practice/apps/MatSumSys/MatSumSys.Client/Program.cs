﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using MatSumSys.Protobuf.Matrix;
using Microsoft.Extensions.Logging.Abstractions;

namespace MatSumSys.Client
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using var channel = GrpcChannel.ForAddress(
                "http://localhost:18081",
                new GrpcChannelOptions()
                {
                    Credentials = ChannelCredentials.Insecure,
                    LoggerFactory = new NullLoggerFactory()
                }
            );
            var mss = new MatrixSumService.MatrixSumServiceClient(channel);
            // await Simple(mss);
            await Several(mss);
        }

        private static async Task Simple(MatrixSumService.MatrixSumServiceClient mss)
        {
            var left = new double[3, 3] { { 1, 1, 1 }, { 1, 6, 1 }, { 1, 2, 3 } };
            var right = new double[3, 3] { { 2, 2, 6 }, { 1, 1, 1 }, { 1, 2, 3 } };

            var result = await mss.SumAsync(
                new PairMatrix()
                {
                    Left = Mapper.Map(left),
                    Right = Mapper.Map(right)
                }
            );

            Console.WriteLine(result.ToString());
        }

        private static async Task Several(MatrixSumService.MatrixSumServiceClient mss)
        {
            var stopwatch = new Stopwatch();
            var tasks = new Dictionary<int, Task>();
            for (var i = 0; i < 100; i++)
            {
                var left = Generator.Gen(100, 150);
                var right = Generator.Gen(100, 150);
                var task = mss.SumAsync(
                    new PairMatrix()
                    {
                        Left = Mapper.Map(left),
                        Right = Mapper.Map(right)
                    }
                );
                tasks[i] = task.ResponseAsync;
            }
            stopwatch.Start();
            await Task.WhenAll(tasks.Values);
            Console.WriteLine($"Elapsed time: {stopwatch.Elapsed}");
        }
    }
}