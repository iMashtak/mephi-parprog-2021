using System;
using System.Threading.Tasks;
using Grpc.Core;
using MatSumSys.Manager.Kafka;
using MatSumSys.Protobuf.Matrix;
using MatSumSys.Worker.Protobuf;
using Serilog;

namespace MatSumSys.Manager.Services
{
    public class MatrixSumServiceImpl : MatrixSumService.MatrixSumServiceBase
    {
        public override async Task<Matrix> Sum(PairMatrix request, ServerCallContext context)
        {
            Log.Information("Handling new request");
            var correlationId = Guid.NewGuid();
            Log.Information("Generated corrId: {CorrelationId}", correlationId);
            await KafkaAdapter.ProduceAsync(
                correlationId,
                new WorkerRq
                {
                    CorrelationId = correlationId.ToString(),
                    RqData = request
                }
            );
            Log.Information("Awaiting for result");
            await foreach (var result in KafkaAdapter.Consume(correlationId))
            {
                Log.Information("Receiving result");
                return result.RsData;
            }
            throw new Exception("Unreachable area");
        }
    }
}