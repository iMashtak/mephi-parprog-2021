using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class Lock
    {
        /// <summary>
        /// Предпочтительный способ использования блокировок - синтаксическая конструкция lock (obj) {}.
        /// Выбирается объект, относительно которого будет производиться блокировка - obj, затем об этот объект
        /// выполяется блокировка. Критическая секция располагается внутри фигурных скобок.
        /// </summary>
        public void CheckThenActUsingMutex()
        {
            var flag = true;

            var thread = new Thread(
                () =>
                {
                    lock (this.GetType())
                    {
                        if (flag)
                        {
                            Thread.Sleep(50);
                            Log($"Ожидалось :: flag={true}");
                            Log($"Получили  :: flag={flag}");
                        }
                    }
                }
            );
            thread.Start();

            Thread.Sleep(25);
            lock (this.GetType())
            {
                flag = false;
            }
            thread.Join();
        }
    }
}