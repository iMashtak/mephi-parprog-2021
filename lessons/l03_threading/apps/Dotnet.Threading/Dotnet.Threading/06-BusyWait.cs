using System.Linq;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class BusyWait
    {
        public void BusyWaitSituation()
        {
            var ok = false;
            
            var threads = Enumerable.Range(0, 16).Select(
                i => new Thread(
                    () =>
                    {
                        while (!ok)
                        {
                            // do nothing
                        }
                        Log($"Thread {i} ends");
                    }
                )
            );
            foreach (var thread in threads)
            {
                thread.Start();
            }
            // Откройте диспетчер задач или вызовите утилиту top и посмотрите на загруз CPU
            Thread.Sleep(30_000);
            ok = true;
            Thread.Sleep(1_000);
        }

        public void BusyWaitResolve()
        {
            // var resetEvent = new ManualResetEvent(false);
            var resetEvent = new AutoResetEvent(false);
            
            var threads = Enumerable.Range(0, 16).Select(
                i => new Thread(
                    () =>
                    {
                        resetEvent.WaitOne();
                        Log($"Thread {i} ends");
                    }
                )
            );
            foreach (var thread in threads)
            {
                thread.Start();
            }
            // Откройте диспетчер задач или вызовите утилиту top и посмотрите на загруз CPU
            Thread.Sleep(1_000);
            resetEvent.Set();
            resetEvent.Set();
            Thread.Sleep(1_000);
        }
    }
}