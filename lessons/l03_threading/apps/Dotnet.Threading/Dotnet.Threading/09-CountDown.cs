using System;
using System.Linq;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class CountDown
    {
        public void Example()
        {
            const int threadCount = 10;
            var random = new Random();
            var countdown = new CountdownEvent(threadCount);

            var threads = Enumerable.Range(0, threadCount).Select(
                i => new Thread(
                    () =>
                    {
                        Thread.Sleep(random.Next(5000, 10000));
                        countdown.Signal();
                        Log($"Thread {i} ends at {DateTime.Now.Ticks}");
                    }
                )
            ).ToList();
            foreach (var thread in threads)
            {
                thread.Start();
            }
            countdown.Wait();
        }
    }
}