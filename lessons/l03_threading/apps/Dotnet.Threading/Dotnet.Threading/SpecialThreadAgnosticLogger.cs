using System;
using System.Collections.Concurrent;

namespace Dotnet.Threading
{
    public class SpecialThreadAgnosticLogger
    {
        private static ConcurrentQueue<object> Result { get; }
        
        static SpecialThreadAgnosticLogger()
        {
            Result = new ConcurrentQueue<object>();
        }
        
        public static void Log(object o) =>
            Result.Enqueue(o);

        public static void Print()
        {
            foreach (var item in Result)
            {
                Console.WriteLine(item);
            }
        }
    }
}