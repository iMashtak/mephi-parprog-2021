using System.Threading;

namespace Dotnet.Threading
{
    /// <summary>
    /// Пример демонстрирует принцип работы модификатора volatile. Для надёжности запускать в Release-конфигурации
    /// </summary>
    public class ValuesCaching
    {
        private int field = 0;

        public void NewTypeOfDeadlock()
        {
            var thread = new Thread(
                () =>
                {
                    this.field = 1;
                    while (this.field == 1) { }
                }
            );
            thread.Start();

            Thread.Sleep(1000);
            // В этот момент поток должен завершиться, ведь field = 0 != 1
            this.field = 0;
            thread.Join();
            // Но нет, программа зависнет. Значение field закэшировалось для другого потока,
            // поэтому её изменение в этом потоке не повлияет на другой.
        }
        
        private volatile int volatiledField = 0;

        public void NoDeadlock()
        {
            var thread = new Thread(
                () =>
                {
                    this.volatiledField = 1;
                    while (this.volatiledField == 1) { }
                }
            );
            thread.Start();

            Thread.Sleep(1000);
            this.volatiledField = 0;
            thread.Join();
            // Всё корректно завершается. Модификатор volatile указывает компилятору, что
            // не надо делать оптимизаций со значением поля. Так что при каждом обращении
            // к volatileField идёт обращение не в кэш потока, а к памяти, где реально лежит
            // значение переменной
            // Подробнее: https://habr.com/ru/post/130318/
        }
    }
}