﻿using System;

using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var basics = new Basics();
            // basics.BackgroundThreads();

            var raceConditions = new RaceConditions();
            // raceConditions.CheckThenAct();

            var mutex = new MutexExamples();
            // mutex.CheckThenActUsingMutex();
            
            var @lock = new Lock();
            // @lock.CheckThenActUsingMutex();

            var deadlock = new Deadlock();
            // deadlock.LiveLock();

            var busyWait = new BusyWait();
            // busyWait.BusyWaitResolve();

            var valuesCaching = new ValuesCaching();
            // valuesCaching.NoDeadlock();

            var atomics = new Atomics();
            // atomics.ReadModifyWriteFailed();
            // atomics.ReadModifyWriteInterlocked();

            var countDown = new CountDown();
            // countDown.Example();

            Print();
        }
    }
}