using System;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class MutexExamples
    {
        /// <summary>
        /// Использование mutex решает проблему Check-Then-Act, если правильно выделить критические секции в коде
        /// </summary>
        public void CheckThenActUsingMutex()
        {
            var mutex = new Mutex();
            var flag = false;

            var thread = new Thread(
                () =>
                {
                    mutex.WaitOne();
                    if (flag)
                    {
                        // Thread.Sleep(50);
                        Log($"Ожидалось :: flag={false}");
                        Log($"Получили  :: flag={flag}");
                    }
                    mutex.ReleaseMutex();
                }
            );
            thread.Start();

            mutex.WaitOne();
            // Thread.Sleep(25);
            flag = true;
            mutex.ReleaseMutex();
            thread.Join();
        }
    }
}