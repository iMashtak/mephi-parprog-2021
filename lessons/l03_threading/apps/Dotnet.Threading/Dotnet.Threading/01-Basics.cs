using System;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class Basics
    {
        public void CurrentThread()
        {
            var thread = Thread.CurrentThread;
            Log($"Имя потока: '{thread.Name}'");
            Log($"Запущен ли поток: {thread.IsAlive}");
            Log($"Приоритет потока: {thread.Priority}"); // 5 уровней
            Log($"Статус потока: {thread.ThreadState}");

            thread.Name = "Main Execution Thread";
            Log($"Имя потока: '{thread.Name}'");
        }

        public void ThreadCreateStartJoin()
        {
            var thread = new Thread(
                () =>
                {
                    var current = Thread.CurrentThread;
                    for (var i = 0; i < 5; i++)
                    {
                        Log($"{current.Name} :: {i}");
                    }
                }
            );
            thread.Start();
            thread.Join();
        }

        public void ThreadsAreReallyRunsParallel()
        {
            void Func(object arg)
            {
                for (var i = 0; i < 10; i++)
                {
                    Log(arg);
                    Thread.Sleep(50);
                }
            }

            var thread1 = new Thread(Func);
            var thread2 = new Thread(Func);

            thread1.Start("First");
            thread2.Start("Second");

            thread1.Join();
            thread2.Join();
        }

        /// <summary>
        /// Фоновый поток - это поток, который позволено завершить при окончании работы приложения.
        /// Не фоновые потоки должны завершиться за время работы программы. 
        /// </summary>
        public void BackgroundThreads()
        {
            var thread = Thread.CurrentThread;
            Log($"Фоновый ли поток: {thread.IsBackground}");

            var backgroundThread = new Thread(
                () =>
                {
                    while (true)
                    {
                        Log(DateTime.Now);
                        Thread.Sleep(500);
                    }
                }
            );
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
            
            Thread.Sleep(3000);
        }
    }
}