using System.Linq;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class Atomics
    {
        private int x;
        
        public void ReadModifyWriteFailed()
        {
            const int bound = 100_000;
            const int threadCount = 20;

            ThreadStart action = () =>
            {
                for (var i = 0; i < bound; i++)
                {
                    this.x += 1;
                }
            };

            var threads = Enumerable.Range(0, threadCount).Select(_ => new Thread(action)).ToList();
            foreach (var thread in threads)
            {
                thread.Start();
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }

            Log($"Ожидалось :: x={bound * threadCount}");
            Log($"Получили  :: x={x}");
        }
        
        // -------------------------
        
        private int y;
        
        public void ReadModifyWriteInterlocked()
        {
            const int bound = 100_000;
            const int threadCount = 20;

            ThreadStart action = () =>
            {
                for (var i = 0; i < bound; i++)
                {
                    Interlocked.Increment(ref this.y);
                }
            };

            var threads = Enumerable.Range(0, threadCount).Select(_ => new Thread(action)).ToList();
            foreach (var thread in threads)
            {
                thread.Start();
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }

            Log($"Ожидалось :: y={bound * threadCount}");
            Log($"Получили  :: y={y}");
        }
    }
}