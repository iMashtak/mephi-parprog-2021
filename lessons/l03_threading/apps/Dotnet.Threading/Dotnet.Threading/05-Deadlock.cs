using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class Deadlock
    {
        public void StupidDeadlock() { Thread.CurrentThread.Join(); }

        public void ClassicDeadlock()
        {
            var locker1 = new object();
            var locker2 = new object();

            var thread1 = new Thread(
                () =>
                {
                    lock (locker1)
                    {
                        Log("Thread 1 blocked locker 1");
                        lock (locker2)
                        {
                            Log("Thread 1 blocked locker 2");
                        }
                    }
                }
            );
            thread1.Start();

            lock (locker2)
            {
                Log("Thread 0 blocked locker 1");
                lock (locker1)
                {
                    Log("Thread 0 blocked locker 2");
                }
            }
        }

        public void LiveLock()
        {
            var locker1 = new object();
            var locker2 = new object();

            var thread1 = new Thread(
                () =>
                {
                    Thread.Sleep(1200);
                    lock (locker1)
                    {
                        Log("Thread 1 blocked locker 1");
                        Thread.Sleep(900);
                        lock (locker2)
                        {
                            Log("Thread 1 blocked locker 2");
                        }
                    }
                }
            );
            thread1.IsBackground = true;
            var thread2 = new Thread(
                () =>
                {
                    Thread.Sleep(500);
                    lock (locker2)
                    {
                        Log("Thread 2 blocked locker 1");
                        Thread.Sleep(500);
                        lock (locker1)
                        {
                            Log("Thread 2 blocked locker 2");
                        }
                    }
                }
            );
            thread2.IsBackground = true;

            thread1.Start();
            thread2.Start();

            Thread.Sleep(5_000);
        }
    }
}