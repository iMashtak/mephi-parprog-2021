using System;
using System.Linq;
using System.Threading;
using static Dotnet.Threading.SpecialThreadAgnosticLogger;

namespace Dotnet.Threading
{
    public class RaceConditions
    {
        private int x;

        public void ReadModifyWrite()
        {
            const int bound = 100_000;
            const int threadCount = 20;

            ThreadStart action = () =>
            {
                for (var i = 0; i < bound; i++)
                {
                    this.x += 1;
                }
            };

            var threads = Enumerable.Range(0, threadCount).Select(_ => new Thread(action)).ToList();
            foreach (var thread in threads)
            {
                thread.Start();
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }

            Log($"Ожидалось :: x={bound * threadCount}");
            Log($"Получили  :: x={x}");
        }

        /// <summary>
        /// Другой вид ошибки, связанный с race condition - значения переменных из внешней области видимости
        /// могут быть изменены другими потоками.
        /// </summary>
        public void CheckThenAct()
        {
            var flag = true;
            
            var thread = new Thread(
                () =>
                {
                    if (flag)
                    {
                        Thread.Sleep(50);
                        Log($"Ожидалось :: flag={true}");
                        Log($"Получили  :: flag={flag}");
                    }
                }
            );
            thread.Start();
            
            Thread.Sleep(25);
            flag = false;
            thread.Join();
        }
    }
}