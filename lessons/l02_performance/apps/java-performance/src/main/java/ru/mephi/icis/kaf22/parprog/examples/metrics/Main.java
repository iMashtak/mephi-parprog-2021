package ru.mephi.icis.kaf22.parprog.examples.metrics;

import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.HTTPServer;
import io.prometheus.client.hotspot.DefaultExports;

import java.io.IOException;
import java.util.Random;
import java.util.Stack;

public class Main {
    static final Gauge braceStackDepth = Gauge.build()
            .name("perfapp_java_brace_stack_depth")
            .help("Depth of the stack of brace")
            .register();

    public static void main(String[] args) throws InterruptedException, IOException {
        DefaultExports.initialize();
        var server = new HTTPServer.Builder()
                .withPort(6558)
                .build();
        var random = new Random();
        var stack = new Stack<Integer>();
        while (true) {
            braceStackDepth.set(stack.size());
            Thread.sleep(1000);
            if (random.nextInt(2) == 0) {
                stack.push(0);
            } else if (stack.size() != 0) {
                stack.pop();
            }
        }
    }
}
