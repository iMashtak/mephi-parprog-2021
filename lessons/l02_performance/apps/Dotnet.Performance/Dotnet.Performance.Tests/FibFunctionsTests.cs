using System.Numerics;
using Dotnet.Performance.Lib;
using Xunit;

namespace Dotnet.Performance.Tests
{
    public class FibFunctionsTests
    {
        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        [InlineData(4, 3)]
        [InlineData(10, 55)]
        public void TestFibs(int number, BigInteger fib)
        {
            var fibNaiveResult = FibFunctions.FibNaive(number);
            var fibMemoizedResult = FibFunctions.FibMemoized(number);
            var fibOptimizedResult = FibFunctions.FibOptimized(number);
            Assert.Equal(fib, fibNaiveResult);
            Assert.Equal(fib, fibMemoizedResult);
            Assert.Equal(fib, fibOptimizedResult);
        }
    }
}