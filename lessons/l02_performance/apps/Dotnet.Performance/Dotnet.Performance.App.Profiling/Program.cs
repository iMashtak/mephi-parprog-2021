﻿using System;
using System.IO;
using Dotnet.Performance.Lib;

namespace Dotnet.Performance.App.Profiling
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Path.Join(
                Directory.GetCurrentDirectory(),
                "..",
                "..",
                "..",
                "..",
                "50Mb.txt"
            );
            var c = new CharCounter('a', 'b', 'c', ' ', '.');
            var _ = c.CountOptimized(path, 4096);
        }
    }
}