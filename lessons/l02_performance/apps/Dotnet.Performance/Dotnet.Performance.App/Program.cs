﻿using System;
using System.Collections.Generic;
using System.Threading;
using Prometheus;
using Prometheus.DotNetRuntime;

namespace Dotnet.Performance.App
{
    internal class Program
    {
        private static readonly Gauge BraceStackDepth = Metrics.CreateGauge(
            "perfapp_dotnet_brace_stack_depth",
            "Depth of the stack of braces"
        );
 
        private static void Main(string[] args)
        {
            using var collector = DotNetRuntimeStatsBuilder
                .Default()
                .StartCollecting();
            var metricServer = new MetricServer(port: 6559);
            metricServer.Start();
            var item = "item";
            var random = new Random();
            var stack = new Stack<string>();
            while (true)
            {
                BraceStackDepth.Set(stack.Count);
                Thread.Sleep(500);
                if (random.Next(2) == 0) { stack.Push(item); }
                else
                {
                    if (stack.Count == 0) { continue; }
                    _ = stack.Pop();
                }
            }
        }
    }
}