using System.IO;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Dotnet.Performance.Lib;

namespace Dotnet.Performance.Benchmarks.Tests
{
    [MemoryDiagnoser]
    [SimpleJob(
        RunStrategy.Monitoring
    )]
    public class CharCounterMacrobenchmarks
    {
        public string Path = System.IO.Path.Join(
            Directory.GetCurrentDirectory(),
            "..",
            "..",
            "..",
            "..",
            "..",
            "..",
            "..",
            "..",
            "50Mb.txt"
        );

        public CharCounter CharCounter;

        [GlobalSetup]
        public void GlobalSetup() { this.CharCounter = new CharCounter( ' ', 'a', 'z'); }

        // [Benchmark]
        public void BenchCharCounterNaive() { this.CharCounter.CountNaive(this.Path); }
        
        [Benchmark]
        // [Arguments(256)]
        // [Arguments(1024)]
        // [Arguments(4096)]
        [Arguments(40960)]
        // [Arguments(4096000)]
        public void BenchCharCounterOptimized(int bufferSize) { this.CharCounter.CountOptimized(this.Path, bufferSize); }
    }
}