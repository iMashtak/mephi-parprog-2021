﻿using BenchmarkDotNet.Running;

namespace Dotnet.Performance.Benchmarks.Tests
{
    internal class Program
    {
        private static void Main(string[] args) { _ = BenchmarkRunner.Run<CharCounterMacrobenchmarks>(); }
    }
}