using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Dotnet.Performance.Lib;

namespace Dotnet.Performance.Benchmarks.Tests
{
    [MemoryDiagnoser]
    [SimpleJob(
        RunStrategy.Throughput,
        launchCount: 1,
        warmupCount: 10,
        targetCount: 10,
        baseline: true
    )]
    public class FibFunctionsMicroBenchmarks
    {
        [Params(5, 10, 15)] 
        public int Number;

        [Benchmark(Baseline = true)]
        public void BenchFibNaive() { FibFunctions.FibNaive(this.Number); }

        [Benchmark]
        public void BenchFibMemoized() { FibFunctions.FibMemoized(this.Number); }

        [Benchmark]
        public void BenchFibOptimized() { FibFunctions.FibOptimized(this.Number); }
    }
}