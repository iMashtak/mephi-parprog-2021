using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("Dotnet.Performance.Benchmarks.Tests")]

namespace Dotnet.Performance.Lib
{
    /// <summary>
    /// For macrobenchmark example
    /// </summary>
    public class CharCounter
    {
        public char[] Chars { get; }

        public CharCounter(params char[] chars) { this.Chars = chars; }

        public IDictionary<char, BigInteger> CountNaive(string path)
        {
            var text = File.ReadAllText(path);
            var store = new Dictionary<char, BigInteger>();
            foreach (var c in this.Chars)
            {
                store[c] = text.Count(x => x == c);
            }
            return store;
        }

        public IDictionary<char, BigInteger> CountOptimized(string path, int bufferSize)
        {
            using var fileStream = File.OpenRead(path);
            var charsSet = this.Chars.ToHashSet();
            var store = new Dictionary<char, BigInteger>();
            var read = 0;
            var buffer = new byte[bufferSize];
            var length = fileStream.Length;
            while (read != length)
            {
                read += fileStream.Read(buffer);
                foreach (var c in Encoding.Default.GetChars(buffer))
                {
                    if (!charsSet.Contains(c)) continue;
                    if (!store.ContainsKey(c)) { store[c] = 0; }
                    store[c] += 1;
                }
            }
            return store;
        }
    }
}