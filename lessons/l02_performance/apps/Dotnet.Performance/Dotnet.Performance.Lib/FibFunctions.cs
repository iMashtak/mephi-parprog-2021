using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Dotnet.Performance.Benchmarks.Tests")]

namespace Dotnet.Performance.Lib
{
    /// <summary>
    /// For microbenchmark example
    /// </summary>
    public class FibFunctions
    {
        public static BigInteger FibNaive(int number)
        {
            return number switch
            {
                0   => 0,
                1   => 1,
                > 1 => FibNaive(number - 1) + FibNaive(number - 2),
                _   => throw new ArgumentOutOfRangeException(nameof(number), number, null)
            };
        }

        public static BigInteger FibMemoized(int number)
        {
            var dict = new Dictionary<int, BigInteger>
            {
                [0] = 0,
                [1] = 1
            };

            BigInteger FibLocal(int n)
            {
                if (dict.ContainsKey(n)) { return dict[n]; }
                dict[n] = FibLocal(n - 1) + FibLocal(n - 2);
                return dict[n];
            }

            return FibLocal(number);
        }

        public static BigInteger FibOptimized(int number)
        {
            if (number == 0) { return 0; }
            var previous = 0;
            var current = 1;
            foreach (var _ in Enumerable.Repeat(0, number - 1))
            {
                (previous, current) = (current, current + previous);
            }
            return current;
        }
    }
}