﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Dotnet.Sockets.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new SocketClient();
            client.Send("hello world!");
        }
    }

    public class SocketClient
    {
        public SocketClient() {}

        public void Send(string msg)
        {
            var buffer = new byte[1024];
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Console.WriteLine("-- Sending request");
            socket.Connect(new IPEndPoint(IPAddress.Parse("192.168.1.46"), 9009));
            socket.Send(Encoding.UTF8.GetBytes(msg));

            Console.WriteLine("-- Receiving response");
            var dataBuilder = new StringBuilder();
            while (true)
            {
                var bytesReceived = socket.Receive(buffer);
                var frame = Encoding.UTF8.GetString(buffer, 0, bytesReceived);
                dataBuilder.Append(frame);
                if (bytesReceived < 1024)
                {
                    break;
                }
            }
            
            var data = dataBuilder.ToString();
            Console.WriteLine(data);
            
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}