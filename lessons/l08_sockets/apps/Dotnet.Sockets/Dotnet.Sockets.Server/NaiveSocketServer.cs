using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Dotnet.Sockets.Server
{
    public class NaiveSocketServer
    {
        public void Listen()
        {
            var buffer = new byte[1024];
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Console.WriteLine("-- Listening on port 9009");

            socket.Bind(new IPEndPoint(IPAddress.Parse("0.0.0.0"), 9009));
            socket.Listen(1);

            while (true)
            {
                Console.WriteLine("-- New request:");
                var socketHandler = socket.Accept();
                var dataBuilder = new StringBuilder();

                while (true)
                {
                    var bytesReceived = socketHandler.Receive(buffer);
                    var frame = Encoding.UTF8.GetString(buffer, 0, bytesReceived);
                    dataBuilder.Append(frame);
                    if (bytesReceived < 1024)
                    {
                        break;
                    }
                }

                var data = dataBuilder.ToString();
                Console.WriteLine(data);

                Console.WriteLine("-- Responding");

                socketHandler.Send(Encoding.UTF8.GetBytes(HTTP_RESPONSE));
                socketHandler.Shutdown(SocketShutdown.Both);
                socketHandler.Close();

                if (data == "exit")
                {
                    break;
                }
            }

            Console.WriteLine("-- Stop listening");
        }

        private string HTTP_RESPONSE = "HTTP/1.1 200 OK\r\nServer: my\r\n\r\nlolkek\r\n";
    }
}