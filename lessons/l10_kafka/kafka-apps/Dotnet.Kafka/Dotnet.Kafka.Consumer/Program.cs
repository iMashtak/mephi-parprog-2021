﻿using System;
using Confluent.Kafka;

namespace Dotnet.Kafka.Consumer
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var config = new ConsumerConfig()
            {
                BootstrapServers = "localhost:29092",
                GroupId = "mytestgroup1",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
            var consumer = new ConsumerBuilder<Null, string>(config).Build();
            consumer.Subscribe("mytesttopic");
            while (true)
            {
                var result = consumer.Consume(TimeSpan.FromMilliseconds(1000));
                if (result == null)
                {
                    Console.WriteLine($"{DateTime.Now} Nothing after 1s");
                }
                else
                {
                    Console.WriteLine($"{DateTime.Now} << {result.Message.Value}");
                }
            }
        }
    }
}