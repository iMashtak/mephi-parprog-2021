﻿using System;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace Dotnet.Kafka.Producer
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var config = new ProducerConfig()
            {
                BootstrapServers = "localhost:29092"
            };
            var producer = new ProducerBuilder<Null, string>(config).Build();
            for (var i = 0; i < 10; ++i)
            {
                var delivery = await producer.ProduceAsync(
                    "mytesttopic", 
                    new Message<Null, string>()
                    {
                        Value = $"message {i}"
                    }
                );
                Console.WriteLine(delivery);
            }
        }
    }
}