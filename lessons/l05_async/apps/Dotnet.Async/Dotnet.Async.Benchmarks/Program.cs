﻿using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Running;

namespace Dotnet.Async.Benchmarks
{
    class Program
    {
        static void Main(string[] args) { _ = BenchmarkRunner.Run<TaskBenchmark>(); }
    }

    [SimpleJob(RunStrategy.Throughput)]
    public class TaskBenchmark
    {
        [Benchmark]
        public async Task WithTask()
        {
            await AdvancedTaskUsage.GetSecureMessageAsync("../../../../../../../../Dotnet.Async/Resources");
        }

        [Benchmark]
        public void NoTask()
        {
            AdvancedTaskUsage.GetSecureMessage("../../../../../../../../Dotnet.Async/Resources");
        }
    }
}