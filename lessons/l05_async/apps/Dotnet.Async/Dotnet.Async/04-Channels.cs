using System;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Dotnet.Async
{
    public class Channels
    {
        public static async Task CalcAsync()
        {
            var channel = Channel.CreateUnbounded<string>();
            var random = new Random();

            async Task Frontend()
            {
                while (true)
                {
                    var cmd = Console.ReadLine();
                    await channel.Writer.WriteAsync(cmd);
                    if (cmd != "q") continue;
                    channel.Writer.Complete();
                    return;
                }
            }

            var frontendTask = Task.Run(Frontend);

            async Task Backend()
            {
                var id = random.Next(20);
                Console.WriteLine($"Backend {id} up");
                await foreach (var cmd in channel.Reader.ReadAllAsync())
                {
                    switch (cmd)
                    {
                        case "disable":
                            return;
                        case "thread":
                            Console.WriteLine($"Backend: Id={id}, Thread={Thread.CurrentThread.ManagedThreadId}");
                            break;
                        default:
                            Console.WriteLine("unknown");
                            break;
                    }
                }
                Console.WriteLine($"Backend {id} down");
            }

            var backends = Enumerable.Repeat(0, 5).Select(_ => Task.Run(Backend));
            // await frontendTask;
            // await Task.WhenAll(backends);
            var all = backends.Append(frontendTask);
            await Task.WhenAll(all);
        }
    }
}