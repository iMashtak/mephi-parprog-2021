using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace Dotnet.Async
{
    public class AsyncCollections
    {
        private static async IAsyncEnumerable<BigInteger> Numbers()
        {
            var c = 0;
            while (true)
            {
                yield return c++;
            }
        }

        public static async Task PrintAllNumbersAsync()
        {
            await foreach (var number in Numbers())
            {
                await Task.Delay(500);
                Console.WriteLine(number);
                if (number == 10) { break; }
            }
        }
    }
}