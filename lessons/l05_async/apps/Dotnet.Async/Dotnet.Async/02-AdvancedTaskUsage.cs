using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dotnet.Async
{
    // Check using Dotnet.Async.Benchmarks::TaskBenchmark
    public class AdvancedTaskUsage
    {
        public static async Task<string> GetSecureMessageAsync(string dir)
        {
            var wordsTask = Task.Run(
                () => File.ReadAllLines($"{dir}/words.txt")
            );
            var keyTask = Task.Run(
                () => File.ReadAllLines($"{dir}/key.txt")
            );
            var results = await Task.WhenAll(wordsTask, keyTask);
            var wordLines = results[0];
            var keyLines = results[1];
            return ApplyKey(wordLines, keyLines);
        }

        public static string GetSecureMessage(string dir)
        {
            var wordLines = File.ReadAllLines($"{dir}/words.txt");
            var keyLines = File.ReadAllLines($"{dir}/key.txt");
            return ApplyKey(wordLines, keyLines);
        }

        private static string ApplyKey(string[] wordLines, string[] keyLines)
        {
            if (wordLines.Length != keyLines.Length)
            {
                throw new Exception("");
            }
            var result = Enumerable.Zip(wordLines, keyLines).Select(
                entry =>
                {
                    var words = entry.First;
                    var keys = entry.Second.Split(".").Select(x => Convert.ToInt32(x)).ToHashSet();
                    var stringBuilder = new StringBuilder();
                    for (var i = 0; i < words.Length; ++i)
                    {
                        if (keys.Contains(i))
                        {
                            stringBuilder.Append(words[i]);
                        }
                    }
                    return stringBuilder.ToString();
                }
            ).Aggregate(new StringBuilder(), (builder, s) => builder.Append(s).Append('\n')).ToString();
            return result;
        }
    }
}