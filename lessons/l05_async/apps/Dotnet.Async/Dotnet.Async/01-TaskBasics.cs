using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using static Dotnet.Async.SpecialThreadAgnosticLogger;

namespace Dotnet.Async
{
    public class TaskBasics
    {
        private static void Some(Stopwatch stopwatch)
        {
            Log(
                $"Into method call: {stopwatch.ElapsedMilliseconds}. ThreadId = {Thread.CurrentThread.ManagedThreadId}"
            );
        }
        
        public static async Task OperateAsync()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Log(
                $"Before call: {stopwatch.ElapsedMilliseconds}. ThreadId = {Thread.CurrentThread.ManagedThreadId}"
            );
            var task = new Task(() => Thread.Sleep(1000));
            task.Start();
            // var task = Task.Delay(1000);
            // Thread.Sleep(1000 + 200);
            // var task = Task.Run(() => Some(stopwatch));
            Log(
                $"After call: {stopwatch.ElapsedMilliseconds}. ThreadId = {Thread.CurrentThread.ManagedThreadId}"
            );
            await task;
            Log(
                $"After await: {stopwatch.ElapsedMilliseconds}. ThreadId = {Thread.CurrentThread.ManagedThreadId}"
            );
        }
    }
}