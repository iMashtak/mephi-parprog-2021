﻿using System;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Dotnet.Async.SpecialThreadAgnosticLogger;

namespace Dotnet.Async
{
    class Program
    {
        private static async Task Main(string[] args)
        {
            // Task.Wait() используется в демонстрационных целях, не делайте так в production!!!
            // TaskBasics.OperateAsync().Wait();

            // Console.WriteLine(await AdvancedTaskUsage.GetSecureMessageAsync("../../../Resources"));

            // AsyncCollections.PrintAllNumbersAsync().Wait();

            // await Channels.CalcAsync();

            await CountingWordsPipeline.Pipe();
            
            Print();
        }
    }
}