using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Dotnet.Async.SpecialThreadAgnosticLogger;

namespace Dotnet.Async
{
    public static class CountingWordsPipeline
    {
        private static ConcurrentDictionary<string, object> _results = new();

        private static async Task ReadFileAsync(
            string                       path,
            ChannelWriter<(int, string)> writer
        )
        {
            using var fileStream = File.OpenText(path);
            var i = 0;
            while (!fileStream.EndOfStream)
            {
                Log($"Read line {i}");
                await writer.WriteAsync((i++, await fileStream.ReadLineAsync()));
            }
            writer.Complete();
        }

        private static async Task Proxy<T>(
            ChannelReader<T>          reader,
            params ChannelWriter<T>[] writers
        )
        {
            await foreach (var item in reader.ReadAllAsync())
            {
                Log("Proxy");
                var i = 0;
                foreach (var writer in writers)
                {
                    Log($"Proxy({++i})");
                    await writer.WriteAsync(item);
                }
            }
            foreach (var writer in writers)
            {
                writer.Complete();
            }
        }

        private static async Task CountWords(
            ChannelReader<(int, string)>     reader
        )
        {
            await foreach (var (number, line) in reader.ReadAllAsync())
            {
                Log($"CountWords {number}");
                var elements = line.Split(" ").Where(x => !string.IsNullOrEmpty(x));
                var count = elements.Count();
                _results[number.ToString()] = count;
            }
        }

        public static async Task CountLines(ChannelReader<(int, string)> reader)
        {
            var result = 0;
            await foreach (var (number, _) in reader.ReadAllAsync())
            {
                Log($"CountLines {number}");
                result++;
            }
            _results["TotalLines"] = result;
        }

        public static async Task Pipe()
        {
            var fileChannel = Channel.CreateBounded<(int, string)>(4);
            var countLinesChannel = Channel.CreateBounded<(int, string)>(6);
            var countWordsChannel = Channel.CreateBounded<(int, string)>(6);

            var readFileTask = ReadFileAsync("../../../Resources/many_words.txt", fileChannel);
            var proxyTask = Proxy<(int, string)>(fileChannel, countLinesChannel, countWordsChannel);
            var countLinesTask = CountLines(countLinesChannel);
            var countWordsTask = CountWords(countWordsChannel);

            await Task.WhenAll(readFileTask, proxyTask, countLinesTask, countWordsTask);
            foreach (var entry in _results)
            {
                Log($"[{entry.Key}] = {entry.Value}");
            }
        }
    }
}