# Занятие 5

> **Тема:** Асинхронность.

## Понятие асинхронности

Многопоточность - это свойство программы, при котором разные её части выполняются в разных потоках исполнения параллельно. Асинхронность - это подход, при котором некоторые вычисления возможно запланировать на выполнение "когда-нибудь" и иметь возможность обработать результат вычисления по факту его выполнения. При этом быть параллельной или многопоточной асинхронная программа не обязана.

Виды реализаций асинхронности:

1. События и callback'и
2. Future/Promise
3. Async/Await

### События и callback'и

Callback - функция обратного вызова, то есть функция, определённая в одном контексте (части программы), но вызываемая в другом по значению:

```cs
void Do(object arg, Action onSuccess, Action onFail) {
    // do something
    if (ok) {
        onSuccess.Invoke();
    } else {
        onFail.Invoke();
    }
}

Do(
    null, 
    () => { Console.Write("Ok"); }, 
    () => { Console.Write("Not ok"); }
)
```

Функция `Do` определяет два коллбека: `onSuccess` и `onFail`. 

Событие - это объект, позволяющий подписать на себя некоторый коллбек множеству подписчиков. То есть структурно событие состоит из пополняемого списка коллбеков. Инициирование события будет состоять в вызове всех коллбеков, подписанных на событие (в C# для событий есть даже ключевое слово `event`.):

```cs
event Action Actions;

Actions += () => {
    Console.Write("First callback");
}
Actions += () => {
    Console.Write("Second callback");
}

Actions.Invoke(); // запускает оба коллбека
```

Проблемы начинаются в тот момент, когда коллбеку необходимы результаты выполнения функции, в которую он был послан:

```cs
object D(Action cd) {
    var result = // somehow
    cd.Invoke(result)
}
object C(Action cc) {
    var result = // somehow
    cc.Invoke(result)
}
object B(Action cb) {
    var result = // somehow
    cb.Invoke(result)
}
object A(Action ca) {
    var result = // somehow
    ca.Invoke(result)
}

A((aResult) => {
    B((bResult) => {
        C((cResult) => {
            D((dResult) => {
                Console.Write(dResult)
            })
        })
    })
})
```

Данный лапшекод называется "callback hell". Подобный подход к реализации асинхронности не является хорошим, но он был первым для многих языков программирования.

### Future/Promise

Promise - это объект, описывающий обещание о том, что некоторое вычисление будет выполнено. Future - это объект, описывающий результат асинхронного вычисления.

```cs
class Future {
    public Future(Func<object, object> func) {}
    public object Get(){} // blocking
}
```

Хорошая статья по этому поводу приведена [по ссылке](https://docs.scala-lang.org/overviews/core/futures.html).

В каждом языке программирования данный подход реализован по-разному, с разными названиями.

- В Scala используются прямые термины - Future и Promise
- В JavaScript используется только Promise, который вобрал в себя все нужные функции
- В Java используется Future (подпадающая под определение Future, но имеющая крайне неудачное API), CompletableFuture (которая одновременно и Promise, и Future)

Многие языки программирования не имеют поддержки данных концепций, в том числе C#.

Рассмотрим принципиальный смысл подхода на примере java:

```java
// создание future-вычисления
CompletableFuture<String> future = CompletableFuture.supplyAsync(
    () -> {
        return "Hello";
    }
)

// some code

// ожидание получения результата вычисления
String result = future.get(); // returns "Hello"
String result = future.get(1000); // returns "Hello"
```

С помощью статического метода `CompletableFuture.supplyAsync` можно запланировать задачу на исполнение и получить объект, описывающий это отложенное вычисление. С помощью метода `get` позже можно обратиться к этому объекту с пожеланием получить результат вычисления. Метод `get` блокирует поток исполнения.

В приведённом фрагменте кода задача неявно была помещена в глобальный тредпул на выполнение.

Важным отличием подхода Future от подхода с callback является возможность вызывать асинхронные вычисления последовательно с более удобным синтаксисом:

```java
var future = CompletableFuture
    .supplyAsync(() -> "Hello")   // future 1
    .thenApply(s -> s + " ")      // future 2
    .thenApply(s -> s + "World"); // future 3

System.out.println(future.get()); // "Hello world"
```

Здесь отсутствует callback hell, так как результат выполнения одной future передаётся на вход другой future. При этом последовательность выполнения будет следующей:

1. На исполнение планируется `future 1`
1. Как только завершается исполнение `future 1`, планируется на исполнение `future 2`
1. Как только завершается исполнение `future 2`, планируется на исполнение `future 3`
1. Информация обо всём этом помещена в объект `future`

### Async/await

Наиболее популярным и прогрессивным из этих трёх является подход async/await. Данная концепция не видоизменяется от языка программирования к языку, поэтому с точки зрения использования везде выглядит одинаково.

Языки, поддерживающие async/await:

1. C# (родоначальник концепции)
1. Python
1. JavaScript

Код следующих двух функций концептуально эквивалентен:

```java
class SomeClass {
    public static CompletableFuture<object> MethodAsync() {
        return CompletableFuture
            .supplyAsync(() -> "Hello")
            .thenApply(s -> s + " ")
            .thenApply(s -> s + "World");
    }
}
```

```cs
class SomeClass {
    public static async Task<object> MethodAsync() {
        var s = await Task.FromResult("Hello");
        s = s + await Task.FromResult(" ");
        s = s + await Task.FromResult("World");
    }
}
```

Класс `Task` является центральным при асинхронном программировании через async/await. `Task` описывает асинхронно выполняющуюся задачу на всём её жизненном цикле. 

Использование async/await, как можно заметить, заставляет реализующую платформу преобразовывать исходный код в некоторый сложный вид, пригодный для исполнения. Как на самом деле происходит преобразование из операторов await к виду из первого фрагмента, можно прочитать [по ссылке](https://habr.com/ru/post/470830/).

## Примеры использования Task

> К коду: `Dotnet.Async`, `Dotnet.Async.Benchmarks`

Из всех примеров можно заметить, что async/await даёт возможность очень простым синтаксисом описывать сложные цепочки взаимосвязанных задач. Future/Promise подход требует использовать методы самих future-объектов, что зачастую не так удобно.

## Асинхронные коллекции и каналы

Практически для любой операции возможно предоставить асинхронный интерфейс. Например, можно асинхронно добавлять элементы в какую-нибудь коллекцию: список, очередь, словарь. Если асинхронность использует многопоточность, то эти структуры данных должны быть thread-safe.

При этом асинхронное получение элемента из коллекции означает то, что в какой-то момент в коллекции может не быть элемента, а потом он может появиться. Получается, что посредством асинхронности возможно строить потенциально бесконечные (по количеству элементов) последовательности.

> К коду: `Dotnet.Async::03-AsyncCollections`

Наиболее типичной структурой для передачи данных из одного потока в другой была очередь. Добавлением к очереди асинхронный интерфейс взаимодействия, получается **канал**.

Канал - это сущность, обладающая следующими свойствами:

1. В канал можно асинхронно записывать данные, причём можно регулировать права на запись
1. Из канала можно асинхронно читать данные, причём можно регулировать права на чтение
1. Канал может иметь ограничение по размеру - буферифированность

> К коду: `Dotnet.Async::04-CountingWordsPipeline`

В рассмотренном примере показывались возможности построения асинхронных вычислительных пайплайнов выполнения вычислений.

Есть два важных аспекта:

1. Каналы могут играть роль очередей задач. Набор тасок может вычитывать один и тот же канал и параллельно выполнять задачи.
1. При помощи каналов удобно определять степень параллелизации тех или иных вычислений. Например, для лёгкой операции можно запустить 5 однотипных тасок, параллельно читающих из канала, а для тяжёлой операции с возможными долгими ожиданиями ввода/вывода можно поднять 20 тасок.
1. С использованием механизма проксирования содержания одного канала на несколько других каналов того же типа можно:
    - параллельно по-разному обрабатывать одни и те же данные;
    - реализовывать модель подписки на события.
